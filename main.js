const {app, BrowserWindow, screen} = require('electron')
    const url = require("url");
    const path = require("path");

    let mainWindow

    function createWindow () {
      const [display] = screen.getAllDisplays()
      console.log(display.bounds.height)

      mainWindow = new BrowserWindow({
        width: display.bounds.width * 3,
        height: display.bounds.height,
        frame: false,
        webPreferences: {
          devTools: false
        }
      })
      mainWindow.setPosition(0, 0)
      mainWindow.loadURL(
        url.format({
          pathname: '//mutant-garden.harmvandendorpel.com#6x2',
          protocol: "https:",
          slashes: true
        })
      );
      // Open the DevTools.
      mainWindow.webContents.openDevTools()

      mainWindow.on('closed', function () {
        mainWindow = null
      })
      
    }

    app.on('ready', createWindow)

    app.on('window-all-closed', function () {
      if (process.platform !== 'darwin') app.quit()
    })

    app.on('activate', function () {
      if (mainWindow === null) createWindow()
    })

